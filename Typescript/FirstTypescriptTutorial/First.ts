// function Test(){
//     var Marks = 33;
//     // var Result = "pass";
//     let Result = "pass";

//     if(Marks < 35) {
//         // var Result = "fail";
//         let Result = "fail";
//         console.log("Inside If: " + Result);
//     }

//     console.log("Inside Test: " + Result);
// }






//first.ts
// enum Result {Pass = 1, Fail = -1, Promoted = 0}

// function Student() { 
//     let iSid: number = 100;
//     let sSame: string = "Xolani";
//     let bIsActive: boolean = true;

//     let arMarks: Array<number> = [45, 67, 89];
//     let total = 0;
//     for(let i of arMarks){
//         total += i;
//     };
//     let Avg = total/ 3.0;

//     let arContacts: string[] = ["9874567323","6723987634","1289348723"];
//     let arDateOfBirth: [number, string,number] = [11, "Dec", 1996];

//     let eResult = Result;
//     this.eResult = function(){
//         if(Avg >= 35){
//             return Result.Pass;
//         }
//         else{
//             return Result.Fail;
//         }
//     }
// }

//Classes and object

class Customer
{
    private CustomerId: number;
    private CustomerName: string;
    CustomerBal: number;
    CustomerSpouse: string;
    // CustomerAddress: Address;

    constructor(CustomerId: number, CustomerName: string, CustomerBal: number, CustomerSpouse: string = "Lilly"
    // CustomerAddress: Address
    ){
        this.CustomerId = CustomerId;
        this.CustomerName= CustomerName;
        this.CustomerBal= CustomerBal;
        this.CustomerSpouse = CustomerSpouse;
        // this.CustomerAddress = CustomerAddress;
    }

    

    display(id?: number){ //? MAKES THE PARAMETER TO BE OPTIONAL
        console.log("Customer Id:" + this.CustomerId +
                    " Name:"+ this.CustomerName +
                    " Bal: " + this.CustomerBal +
                    // " Address: " + this.CustomerAddress.Address + " " + this.CustomerAddress.City);
                     " Spouse: " + this.CustomerSpouse);
    }

    deposit(amt: number): number{
        this.CustomerBal = this.CustomerBal + amt;
        return this.CustomerBal;
    }
}

var c = new Customer(1,"Xolani", 6000); 
c.display()

//passing objects as a parameter
// class Address{
//     AddressId: number;
//     Address: string;
//     City: string;
//     State: string;
//     Country: string;

//     constructor(AddressId: number, Address: string, 
//         City: string, State:string, Country: string){
//             this.AddressId = AddressId;
//             this.Address = Address;
//             this.City = City;
//             this.State = State;
//             this.Country = Country; 
//         }
// }