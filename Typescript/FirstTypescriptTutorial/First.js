// function Test(){
//     var Marks = 33;
//     // var Result = "pass";
//     let Result = "pass";
//     if(Marks < 35) {
//         // var Result = "fail";
//         let Result = "fail";
//         console.log("Inside If: " + Result);
//     }
//     console.log("Inside Test: " + Result);
// }
//first.ts
// enum Result {Pass = 1, Fail = -1, Promoted = 0}
// function Student() { 
//     let iSid: number = 100;
//     let sSame: string = "Xolani";
//     let bIsActive: boolean = true;
//     let arMarks: Array<number> = [45, 67, 89];
//     let total = 0;
//     for(let i of arMarks){
//         total += i;
//     };
//     let Avg = total/ 3.0;
//     let arContacts: string[] = ["9874567323","6723987634","1289348723"];
//     let arDateOfBirth: [number, string,number] = [11, "Dec", 1996];
//     let eResult = Result;
//     this.eResult = function(){
//         if(Avg >= 35){
//             return Result.Pass;
//         }
//         else{
//             return Result.Fail;
//         }
//     }
// }
//Classes and object
var Customer = /** @class */ (function () {
    function Customer(CustomerId, CustomerName, CustomerBal, /*CustomerSpouse: string = "Lilly"*/ CustomerAddress) {
        this.CustomerId = CustomerId;
        this.CustomerName = CustomerName;
        this.CustomerBal = CustomerBal;
        // this.CustomerSpouse = CustomerSpouse;
        this.CustomerAddress = CustomerAddress;
    }
    Customer.prototype.display = function (id) {
        console.log("Customer Id:" + this.CustomerId +
            " Name:" + this.CustomerName +
            " Bal: " + this.CustomerBal +
            " Address: " + this.CustomerAddress.Address + " " + this.CustomerAddress.City);
        // + " Spouse: " + this.CustomerSpouse);
    };
    Customer.prototype.deposit = function (amt) {
        this.CustomerBal = this.CustomerBal + amt;
        return this.CustomerBal;
    };
    return Customer;
}());
// var c = new Customer(1,"Xolani", 6000); 
// c.display()
//passing objects as a parameter
var Address = /** @class */ (function () {
    function Address(AddressId, Address, City, State, Country) {
        this.AddressId = AddressId;
        this.Address = Address;
        this.City = City;
        this.State = State;
        this.Country = Country;
    }
    return Address;
}());
