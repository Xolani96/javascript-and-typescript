var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Employee = /** @class */ (function () {
    function Employee(EmpId, Name, BasicSalary) {
        this.EmpId = EmpId;
        this.Name = Name;
        this.BasicSalary = BasicSalary;
    }
    Employee.prototype.display = function () {
        console.log("Eid: " + this.EmpId + " Name: " + this.Name
            + " basicSalary: " + this.BasicSalary);
    };
    return Employee;
}());
var Lecturer = /** @class */ (function (_super) {
    __extends(Lecturer, _super);
    function Lecturer(EmpId, Name, BasicSalary, Subject) {
        var _this = _super.call(this, EmpId, Name, BasicSalary) || this;
        _this.Subject = Subject;
        return _this;
    }
    Lecturer.prototype.calculateSalary = function (hours) {
        var salary;
        salary = this.BasicSalary * hours + (this.BasicSalary * hours) * (2.5 / 100);
        console.log("Lecturer's salary " + salary);
    };
    Lecturer.prototype.display = function () {
        _super.prototype.display.call(this);
        console.log(" Subject: " + this.Subject);
    };
    return Lecturer;
}(Employee));
var LabAssistant = /** @class */ (function (_super) {
    __extends(LabAssistant, _super);
    function LabAssistant(EmpId, Name, BasicSalary, LabNumber) {
        var _this = _super.call(this, EmpId, Name, BasicSalary) || this;
        _this.LabNumber = LabNumber;
        return _this;
    }
    LabAssistant.prototype.calculateSalary = function (hours) {
        var salary;
        salary = this.BasicSalary * hours + (this.BasicSalary * hours) * 1.8 / 100;
        console.log("LabAssistant's salary " + salary);
    };
    LabAssistant.prototype.display = function () {
        _super.prototype.display.call(this);
        console.log(" Subject: " + this.LabNumber);
    };
    return LabAssistant;
}(Employee));
function test(x) {
    var E;
    if (x == 1) {
        E = new Lecturer(100, "Xolani", 6000, "Typescript");
    }
    else if (x == 2) {
        E = new LabAssistant(101, "Thabo", 7000, "L-342");
    }
    E.display();
    E.calculateSalary(18);
}
