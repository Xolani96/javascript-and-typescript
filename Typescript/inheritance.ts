abstract class Employee{
    protected EmpId: number;
    protected Name: string;
    protected BasicSalary: number;

    constructor(EmpId: number, Name: string, BasicSalary: number){
        this.EmpId = EmpId;
        this.Name = Name;
        this.BasicSalary = BasicSalary;
    }

    abstract calculateSalary(hours: number):void;

    display() {
        console.log("Eid: " + this.EmpId + " Name: " + this.Name 
        + " basicSalary: " + this.BasicSalary);
    }
}
class Lecturer extends Employee {//extends used for inheritance

    calculateSalary(hours: number): void {
        let salary: number;
        salary = this.BasicSalary * hours + (this.BasicSalary*hours)*(2.5 / 100);
        console.log("Lecturer's salary " + salary);
    } 

    protected Subject: string;
    constructor(EmpId: number, Name: string, BasicSalary: number, Subject: string){
        super(EmpId,Name,BasicSalary);
        this.Subject = Subject;
    }

    display(){
        super.display();
        console.log(" Subject: " + this.Subject);
    }
}
class LabAssistant extends Employee {//extends used for inheritance

    calculateSalary(hours: number): void {
        let salary: number;
        salary = this.BasicSalary * hours + (this.BasicSalary*hours)*1.8 / 100;
        console.log("LabAssistant's salary " + salary);
    } 

    protected LabNumber: string;
    constructor(EmpId: number, Name: string, BasicSalary: number, LabNumber: string){
        super(EmpId,Name,BasicSalary);
        this.LabNumber = LabNumber;
    }

    display(){
        super.display();
        console.log(" Subject: " + this.LabNumber);
    }
}
function test(x: number){
    let E: Employee;
    if(x == 1){
        E = new Lecturer(100, "Xolani", 6000, "Typescript");
    }else if(x==2){
        E = new LabAssistant(101,"Thabo",7000,"L-342");
    }

    E.display();
    E.calculateSalary(18);
}


//namespaces
// namespace common{
//     export namespace Login { //export - allows the class/namespace/ ..etc to be accessible from outside
//         export class User {
    
//         }
//     }
// }

// var u = new common.Login.User();
