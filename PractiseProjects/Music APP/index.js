window.addEventListener('load', () => {
    //after the content is loaded , this code will run
    const sounds = document.querySelectorAll(".sound");
    const pads = document.querySelectorAll(".pads div")
    const visual = document.querySelector('visual');
    const colors = [
        "#606060",
        "#d3d3d3",
        "rebeccapurple",
        "red",
        "palevioletred",
        "navajowhite"
    ]
    
    // console.log(sounds[0]);

    //lets get going with the sound here
    pads.forEach((pad, index ) => {
        pad.addEventListener('click', function(){
            sounds[index].currentTime = 0;
            sounds[index].play();

            createbubbles(index);
        })  
    })

    //create a function that makes bubbles
    const createbubbles = index => {
        const bubble = document.createElement("div");
        visual.appendChild(bubble);
        bubble.style.background = colors[index];
        bubble.style.animation = `jump 1s ease`;
        bubble.addEventListener('animationend', function() {
            visual.removeChild(this);
        })
    };
})